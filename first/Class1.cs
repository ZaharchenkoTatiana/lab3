﻿using System;

namespace first {
public class Class1{ 
public double[] solve(double a,double b, double c){
    double e = 1e-5;
    double D = b*b - 4*a*c;
    
    if (Math.Abs(a) <= e) {
                throw new ArgumentException();
            }

     if(Math.Abs(D) < e ) 
            {
                return new double[] {-b/(2*a)};
            }
            else if(D<-e) {
                return new double[0];
            }
            else { 
                double d = Math.Sqrt(D);
                return new double[] {(-b - d)/(2*a),(-b + d)/(2*a)};
            }
            
        }
    }
}