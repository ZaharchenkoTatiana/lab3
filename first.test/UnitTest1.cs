using System;
using Xunit;
using first;

internal class MyAssert {
public static void Equal(Double expected, Double actual, Double eps) {
if (Math.Abs(expected - actual) > eps) {
throw new DoubleExeption(expected: expected.ToString(), actual: actual.ToString(), message: "");
        }
    }
}

internal class DoubleExeption: Xunit.Sdk.AssertActualExpectedException {
internal DoubleExeption(string expected, string actual, string message)
: base(expected, actual, message) {}
}


namespace first.test {
    public class UnitTest1{
[Fact]
        public void DoubleSolution()
        {
            Class1 c = new Class1();
            double[] result = c.solve(1, -2, 1);
            MyAssert.Equal(1, result[0], 1e-5);
        }



[Fact]
        public void TwoRoot()
        {
            Class1 c = new Class1();
            double[] result = c.solve(1, 0, -1);
            Array.Sort(result);
            MyAssert.Equal(-1, result[0], 1e-5);
            MyAssert.Equal(1, result[1], 1e-5);
        }
        
[Fact]
        public void ZeroRoot()
        {
            Class1 c = new Class1();
            double[] result = c.solve(1, 0, 1);
            MyAssert.Equal(0, result.Length, 1e-5);
        }
[Fact]
        public void aequalszero()
        {
            Class1 c = new Class1();
            Assert.Throws<ArgumentException>(() => c.solve(1e-5, 1, 1));
        }
        
    }
    }
